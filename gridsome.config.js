/**
 * Spletna stran za šolske knjige
 * Copyright (C) 2021 Riccardo Riccio <rickyriccio@zoho.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

module.exports = {
  siteName: "Knjige • Potep",
  titleTemplate: "%s • Knjige • Potep",
  siteUrl: "https://knjige.potep.org",
  plugins: [
    { use: "gridsome-plugin-svg" },
    {
      use: "@gridsome/source-filesystem",
      options: {
        path: "./data/books/*.md",
        typeName: "Book",
        pathPrefix: "/book",
        remark: {}
      }
    },
    {
      use: `gridsome-plugin-netlify-cms`,
      options: {
        publicPath: `/admin`,
        modulePath: `src/admin/index.js`
      }
    },
    {
      use: "gridsome-plugin-flexsearch",
      options: {
        searchFields: ["name"],
        collections: [
          {
            typeName: "Book",
            indexName: "Book",
            fields: ["name", "code", "path"]
          }
        ]
      }
    },
    {
      use: "@gridsome/plugin-sitemap",
      options: {
        config: {
          "/books/*": {
            changefreq: "weekly",
            priority: 0.7
          }
        }
      }
    },
    {
      use: "gridsome-plugin-robots",
      options: {
        policy: [{ userAgent: "*", allow: "/" }]
      }
    }
  ],
  transformers: { remark: {} },
  templates: {
    Book: "/books/:fileInfo__name"
  },
  permalinks: {
    slugify: {
      use: "@sindresorhus/slugify",
      options: {
        decamelize: false
      }
    }
  }
};
