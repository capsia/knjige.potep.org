module.exports = {
  extends: [
    "plugin:vue/recommended",
    "plugin:prettier/recommended",
    // Do not add `'prettier/vue'` if you don't want to use prettier for `<template>` blocks
    "prettier"
  ],

  rules: {
    "prettier/prettier": "error",
    "vue/no-v-html": "off"
  }
};
