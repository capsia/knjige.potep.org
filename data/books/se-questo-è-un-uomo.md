---
name: Se questo è un uomo
picture: https://img.ibs.it/images/9788858420430_0_221_0_75.jpg
author: Primo Levi
---
codice libro : 405, 1144, 1343, 1482

Un tragico, ma eccezionale racconto sulla drammatica esperienza di Auschwitz.