---
name: 'Racconti Matematici'
---

Questo libro rende la logica divertente con racconti che cambiano il tuo modo di comprendere la matematica.
